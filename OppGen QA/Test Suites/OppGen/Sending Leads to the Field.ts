<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Sending Leads to the Field</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>b1a4866b-9f10-4ae0-90f4-c18a420caf1e</testSuiteGuid>
   <testCaseLink>
      <guid>c7684531-bcbd-43e6-b401-d1264929f2cb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Sending Leads to the Field/Auto-Assign due to no SLAs available</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>76456a74-f9ba-47c5-b896-76efb58c7653</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Sending Leads to the Field/Auto-Assign using Assign Lead button on WT page</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
