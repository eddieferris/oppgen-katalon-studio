import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

WebUI.navigateToUrl('http://ci-main-ygl/')

WebUI.setText(findTestObject('Object Repository/SpyPage_Opportunity Generator/YGL Test/input_User Name_username'), 'chadsto')

WebUI.setEncryptedText(findTestObject('Object Repository/SpyPage_Opportunity Generator/YGL Test/input_Password_password'), 
    'oagVfJEzz+A=')

WebUI.click(findTestObject('Object Repository/SpyPage_Opportunity Generator/YGL Test/input_Forgot password_Login'))

WebUI.click(findTestObject('Object Repository/SpyPage_Opportunity Generator/YGL Test/a_Add Lead'))

WebUI.click(findTestObject('Object Repository/SpyPage_Opportunity Generator/YGL Test/input_Disclosure Statement_Che'))

WebUI.click(findTestObject('Object Repository/SpyPage_Opportunity Generator/YGL Test/input_Payoff_btnDisclosure'))

WebUI.setText(findTestObject('Object Repository/SpyPage_Opportunity Generator/YGL Test/input_PostalCode_desiredCity'), 'Denver')

WebUI.setText(findTestObject('Object Repository/SpyPage_Opportunity Generator/YGL Test/input_PostalCode_desiredState'), 
    'co')

WebUI.click(findTestObject('Object Repository/SpyPage_Opportunity Generator/YGL Test/input_Unable to recognize loca'))

WebUI.selectOptionByValue(findTestObject('Object Repository/SpyPage_Opportunity Generator/YGL Test/select_- select -1025507590'), 
    '10', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/SpyPage_Opportunity Generator/YGL Test/select_- select -JanuaryFebrua'), 
    'January', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/SpyPage_Opportunity Generator/YGL Test/select_- select -2019202020212'), 
    '2033', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/SpyPage_Opportunity Generator/YGL Test/select_- select -Assisted Livi'), 
    '38', true)

WebUI.setText(findTestObject('SpyPage_Opportunity Generator/YGL Test/input_EnterLocationName'), 'Apple House (license revoked)')

WebUI.selectOptionByValue(findTestObject('Object Repository/SpyPage_Opportunity Generator/YGL Test/select_- select -Yes short ter'), 
    '80', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/SpyPage_Opportunity Generator/YGL Test/select_10000 and over9000-9999'), 
    '17', true)

WebUI.setText(findTestObject('Object Repository/SpyPage_Opportunity Generator/YGL Test/input_Name_primaryContactfirst'), 
    'Eddie')

WebUI.setText(findTestObject('Object Repository/SpyPage_Opportunity Generator/YGL Test/input_Name_primaryContactlastN'), 
    'ferris')

WebUI.selectOptionByValue(findTestObject('Object Repository/SpyPage_Opportunity Generator/YGL Test/select_- select  -AuntBrother-'), 
    '17', true)

WebUI.setText(findTestObject('Object Repository/SpyPage_Opportunity Generator/YGL Test/input_Home Phone_primaryContac'), 
    '2066664321')

WebUI.setText(findTestObject('Object Repository/SpyPage_Opportunity Generator/YGL Test/input_Name_residentContactfirs'), 
    'chelsea')

WebUI.setText(findTestObject('Object Repository/SpyPage_Opportunity Generator/YGL Test/input_Name_residentContactlast'), 
    'ferris')

WebUI.selectOptionByValue(findTestObject('Object Repository/SpyPage_Opportunity Generator/YGL Test/select_- select -FemaleMaleCou'), 
    'F', true)

WebUI.click(findTestObject('Object Repository/SpyPage_Opportunity Generator/YGL Test/input_Do Not Call_btnSave'))

WebUI.click(findTestObject('Object Repository/SpyPage_Opportunity Generator/YGL Test/div_chelsea ferris (Female)'))

WebUI.closeBrowser()

