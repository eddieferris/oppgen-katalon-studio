import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

'Navigate to OppGen QA login page'
WebUI.navigateToUrl('http://usw2wbqa1:8080/Account/Login')

WebUI.deleteAllCookies()

'Input Username (OppGen)'
WebUI.setText(findTestObject('Page_Opportunity Generator - Login/input_Username_OppGen'), 'eddief')

'Input Password (OppGen)'
WebUI.setEncryptedText(findTestObject('Page_Opportunity Generator - Login/input_Password_OppGen'), 'xSaxR5Zc964=')

'Click \'Sign In\' button'
WebUI.click(findTestObject('Page_Opportunity Generator - Login/btn_SignIn_OppGen'))

'Click "Launch Five9" link'
WebUI.click(findTestObject('Page_Opportunity Generator - Connection/link_Launch Five9'))

'Focus on Five9 ADT window'
WebUI.switchToWindowTitle('Five9 Toolbar', FailureHandling.CONTINUE_ON_FAILURE)

'Input Username (Five9 ADT)'
WebUI.setText(findTestObject('Page_Five9 - Main/input_Username_Five9'), 'eddie.ferris@aplaceformom.com', FailureHandling.CONTINUE_ON_FAILURE)

'Input Password (Five9 ADT)'
WebUI.setEncryptedText(findTestObject('Page_Five9 - Main/input_Password_Five9'), 'zgMKZ/4x+qIZ/2ovhLJ/rw==', FailureHandling.CONTINUE_ON_FAILURE)

'Input StationID (Five9 ADT)'
WebUI.setText(findTestObject('Page_Five9 - Main/input_StationID_Five9'), '1816757', FailureHandling.CONTINUE_ON_FAILURE)

'Click \'Login\' button'
WebUI.click(findTestObject('Page_Five9 - Main/btn_Login'), FailureHandling.CONTINUE_ON_FAILURE)

'Enable "TEST QA OppGen OB" skill  -- (NonPro OB)'
WebUI.check(findTestObject('Page_Five9 - Skills/chkbox_TEST QA OppGen OB'), FailureHandling.CONTINUE_ON_FAILURE)

'Click \'Ok\' button'
WebUI.click(findTestObject('Page_Five9 - Main/btn_Ok'), FailureHandling.CONTINUE_ON_FAILURE)

'Focus on OppGen QA window'
WebUI.switchToWindowTitle('Opportunity Generator', FailureHandling.CONTINUE_ON_FAILURE)

'Set status to "Available"'
WebUI.click(findTestObject('Page_Opportunity Generator - Connection/btn_SetStatusToAvailable'), FailureHandling.CONTINUE_ON_FAILURE)

'Wait for "...disclaimer" element to become visible'
WebUI.waitForElementVisible(findTestObject('Page_Opportunity Generator - Connection/chkBox_disclaimer'), 300)

'Check "...disclaimer"'
WebUI.click(findTestObject('Page_Opportunity Generator - Connection/chkBox_disclaimer'))

'Wait for "Continue To Duplicate Processing" button to be clickable'
WebUI.waitForElementClickable(findTestObject('Page_Opportunity Generator - Connection/btn_ContinueToDuplicateProcessing'), 
    10, FailureHandling.OPTIONAL)

'Click "Continue To Duplicate Processing" button (or Skip)'
WebUI.click(findTestObject('Page_Opportunity Generator - Connection/btn_ContinueToDuplicateProcessing'), FailureHandling.OPTIONAL)

'Click "None of these Apply" button (or Skip)'
WebUI.click(findTestObject('Page_Opportunity Generator - Duplicate Check/btn_NoneOfTheseApply'), FailureHandling.OPTIONAL)

'Wait for "The Current Caller Is Not Associated..." button to become clickable'
WebUI.waitForElementClickable(findTestObject('Page_Opportunity Generator - Duplicate Check/btn_TheCurrentCallerIsNotAssociated'), 
    300)

'Click "The current caller is not associated.." button (or Skip)'
WebUI.click(findTestObject('Page_Opportunity Generator - Duplicate Check/btn_TheCurrentCallerIsNotAssociated'), FailureHandling.OPTIONAL)

'Click "Continue To Confirm Interest" button'
WebUI.click(findTestObject('Page_Opportunity Generator - Connection/btn_ContinueToConfirmInterest'), FailureHandling.OPTIONAL)

'Click "SELF" option label (radio button element doesn\'t work)'
WebUI.click(findTestObject('Page_Opportunity Generator - Interest/link_SELFoption'))

'Click "Afford 2000 Yes" radio button'
WebUI.click(findTestObject('Page_Opportunity Generator - Interest/radioBtn_Afford2000Yes'))

WebUI.scrollToElement(findTestObject(null), 0)

'Click "Continue To Contact Info" button'
WebUI.click(findTestObject('Page_Opportunity Generator - Interest/btn_ContinueToContactInfo'))

'Wait 5 seconds max for page to load'
WebUI.waitForAngularLoad(5)

'Click "Verify" button'
WebUI.click(findTestObject('Page_Opportunity Generator - Contact Info/btn_Verify'), FailureHandling.STOP_ON_FAILURE)

'Wait for "Continue To Warm Transfer" button to become clickable'
WebUI.waitForElementClickable(findTestObject('Page_Opportunity Generator - Interest/btn_ContinueToWarmTransfer'), 300)

'Click "Continue To Warm Transfer" button'
WebUI.click(findTestObject('Page_Opportunity Generator - Interest/btn_ContinueToWarmTransfer'))

'Wait for "Request Warm Transfer" button to become clickable'
WebUI.waitForElementClickable(findTestObject('Page_Opportunity Generator - Warm Transfer/btn_RequestWarmTransfer'), 300)

'Click "Request Warm Transfer" button'
WebUI.click(findTestObject('Page_Opportunity Generator - Warm Transfer/btn_RequestWarmTransfer'))

WebUI.acceptAlert()

WebUI.acceptAlert()

WebUI.waitForElementPresent(findTestObject('Page_Opportunity Generator - Warm Transfer/div_SLAInfoPanel'), 300)

WebUI.verifyElementVisible(findTestObject('Page_Opportunity Generator - Warm Transfer/span_SLAName'), FailureHandling.STOP_ON_FAILURE)

WebUI.verifyElementVisible(findTestObject('Page_Opportunity Generator - Warm Transfer/span_SLANumber'))

'Wait for "Go To Wrap-Up Page" button to become clickable'
WebUI.waitForElementClickable(findTestObject('Page_Opportunity Generator - Warm Transfer/btn_GoToWrapUpPage'), 300)

'Click "Go To Wrap-Up Page" button'
WebUI.click(findTestObject('Page_Opportunity Generator - Warm Transfer/btn_GoToWrapUpPage'))

'Focus on Five9 ADT window'
WebUI.switchToWindowTitle('Five9 Toolbar')

'Focus on the Status (Ready) drop down'
WebUI.focus(findTestObject('Page_Five9 - Main/span_Ready'))

'Click the Status drop down '
WebUI.click(findTestObject('Page_Five9 - Main/span_Ready'))

'Select the "Break" option'
WebUI.click(findTestObject('Page_Five9 - Main/span_Break'))

'Focus on OppGen window'
WebUI.switchToWindowTitle('Opportunity Generator')

'Wait for "Finish Call" button to become clickable'
WebUI.waitForElementClickable(findTestObject('Page_Opportunity Generator - Summary/btn_FinishCall'), 300)

'Click "Finish Call" button'
WebUI.click(findTestObject('Page_Opportunity Generator - Summary/btn_FinishCall'))

'Wait for "Finish Call" button to become NOT clickable'
WebUI.waitForElementNotClickable(findTestObject('Page_Opportunity Generator - Summary/btn_FinishCall'), 300)

'Switch focus to Five9 ADT window'
WebUI.switchToWindowTitle('Five9 Toolbar')

'Focus on Status (Break) drop down'
WebUI.focus(findTestObject('Page_Five9 - Main/span_Break'))

'Click the Status drop down'
WebUI.click(findTestObject('Page_Five9 - Main/span_Break_CurrentStatus'))

'Select the "Logout" option'
WebUI.click(findTestObject('Page_Five9 - Main/span_Logout'))

'Click the "Confirm Logout" button'
WebUI.click(findTestObject('Page_Five9 - Main/btn_ConfirmLogout'))

WebUI.closeBrowser()

