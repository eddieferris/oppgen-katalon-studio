import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.logging.KeywordLogger as KeywordLogger

KeywordLogger log = new KeywordLogger()

WebUI.openBrowser('')

WebUI.openBrowser('')

'Navigate to OppGen QA login page'
WebUI.navigateToUrl('http://usw2wbqa1:8080/Account/Login')

not_run: WebUI.deleteAllCookies()

'Input Username (OppGen)'
WebUI.setText(findTestObject('Page_Opportunity Generator - Login/input_Username_OppGen'), 'eddief')

'Input Password (OppGen)'
WebUI.setEncryptedText(findTestObject('Page_Opportunity Generator - Login/input_Password_OppGen'), 'xSaxR5Zc964=')

'Click \'Sign In\' button'
WebUI.click(findTestObject('Page_Opportunity Generator - Login/btn_SignIn_OppGen'))

'Click "Launch Five9" link'
WebUI.click(findTestObject('Page_Opportunity Generator - Connection/link_Launch Five9'))

'Focus on Five9 ADT window'
WebUI.switchToWindowTitle('Five9 Toolbar')

'Input Username (Five9 ADT)'
WebUI.setText(findTestObject('Page_Five9 - Main/input_Username_Five9'), 'eddie.ferris@aplaceformom.com', FailureHandling.CONTINUE_ON_FAILURE)

'Input Password (Five9 ADT)'
WebUI.setEncryptedText(findTestObject('Page_Five9 - Main/input_Password_Five9'), 'zgMKZ/4x+qIZ/2ovhLJ/rw==', FailureHandling.CONTINUE_ON_FAILURE)

'Input StationID (Five9 ADT)'
WebUI.setText(findTestObject('Page_Five9 - Main/input_StationID_Five9'), '1816757', FailureHandling.CONTINUE_ON_FAILURE)

'Click \'Login\' button'
WebUI.click(findTestObject('Page_Five9 - Main/btn_Login'), FailureHandling.CONTINUE_ON_FAILURE)

'Enable "TEST QA OppGen OB" skill  -- (NonPro OB)'
WebUI.check(findTestObject('null'), FailureHandling.CONTINUE_ON_FAILURE)

'Click \'Ok\' button'
WebUI.click(findTestObject('Page_Five9 - Main/btn_Ok'), FailureHandling.CONTINUE_ON_FAILURE)

'Focus on OppGen QA window'
WebUI.switchToWindowTitle('Opportunity Generator')

'Set status to "Available"'
WebUI.click(findTestObject('Page_Opportunity Generator - Connection/btn_SetStatusToAvailable'))

'Wait for "...disclaimer" element to become visible'
WebUI.waitForElementVisible(findTestObject('Page_Opportunity Generator - Connection/chkBox_disclaimer'), 300)

'Check "...disclaimer"'
WebUI.click(findTestObject('Page_Opportunity Generator - Connection/chkBox_disclaimer'))

'Wait for "Continue To Duplicate Processing" button to be clickable'
WebUI.waitForElementClickable(findTestObject('Page_Opportunity Generator - Connection/btn_ContinueToDuplicateProcessing'), 
    300, FailureHandling.OPTIONAL)

'Click "Continue To Duplicate Processing" button (or Skip)'
WebUI.click(findTestObject('Page_Opportunity Generator - Connection/btn_ContinueToDuplicateProcessing'), FailureHandling.OPTIONAL)

WebUI.focus(findTestObject('null'))

'Click "None of these Apply" button (or Skip)'
WebUI.click(findTestObject('Page_Opportunity Generator - Duplicate Check/btn_NoneOfTheseApply'), FailureHandling.OPTIONAL)

WebUI.waitForElementClickable(findTestObject('Page_Opportunity Generator - Duplicate Check/btn_TheCurrentCallerIsNotAssociated'), 
    300)

'Navigate to OppGen QA login page'
WebUI.navigateToUrl('http://usw2wbqa1:8080/Account/Login')

'Input Username (OppGen)'
WebUI.setText(findTestObject('Page_Opportunity Generator - Login/input_Username_OppGen'), 'eddief')

log.logWarning('This is a test for logwarning')

'Input Password (OppGen)'
WebUI.setEncryptedText(findTestObject('Page_Opportunity Generator - Login/input_Password_OppGen'), 'xSaxR5Zc964=')

'Click \'Sign In\' button'
WebUI.click(findTestObject('Page_Opportunity Generator - Login/btn_SignIn_OppGen'))

'Click "Launch Five9" link'
WebUI.click(findTestObject('Page_Opportunity Generator - Connection/link_Launch Five9'))

'Focus on Five9 ADT window'
WebUI.switchToWindowTitle('Five9 Toolbar')

'Input Username (Five9 ADT)'
WebUI.setText(findTestObject('Page_Five9 - Main/input_Username_Five9'), 'eddie.ferris@aplaceformom.com', FailureHandling.CONTINUE_ON_FAILURE)

'Input Password (Five9 ADT)'
WebUI.setEncryptedText(findTestObject('Page_Five9 - Main/input_Password_Five9'), 'zgMKZ/4x+qIZ/2ovhLJ/rw==', FailureHandling.CONTINUE_ON_FAILURE)

'Input StationID (Five9 ADT)'
WebUI.setText(findTestObject('Page_Five9 - Main/input_StationID_Five9'), '1816757', FailureHandling.CONTINUE_ON_FAILURE)

'Click \'Login\' button'
WebUI.click(findTestObject('Page_Five9 - Main/btn_Login'), FailureHandling.CONTINUE_ON_FAILURE)

'Enable "TEST QA OppGen OB" skill  -- (NonPro OB)'
WebUI.check(findTestObject('null'), FailureHandling.CONTINUE_ON_FAILURE)

'Click \'Ok\' button'
WebUI.click(findTestObject('Page_Five9 - Main/btn_Ok'), FailureHandling.CONTINUE_ON_FAILURE)

'Focus on OppGen QA window'
WebUI.switchToWindowTitle('Opportunity Generator')

'Set status to "Available"'
WebUI.click(findTestObject('Page_Opportunity Generator - Connection/btn_SetStatusToAvailable'))

WebUI.click(findTestObject(null))

