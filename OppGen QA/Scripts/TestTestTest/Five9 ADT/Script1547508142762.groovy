import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.support.ui.Select as Select

WebUI.openBrowser('')

'Navigate to OppGen QA login page'
WebUI.navigateToUrl('http://usw2wbqa1:8080/Account/Login')

WebUI.setText(findTestObject('null'), 'eddief')

WebUI.setEncryptedText(findTestObject('null'), 'xSaxR5Zc964=')

WebUI.click(findTestObject('null'))

WebUI.click(findTestObject('null'))

WebUI.switchToWindowTitle('Five9 Toolbar')

WebUI.setText(findTestObject('null'), 'eddie.ferris@aplaceformom.com')

WebUI.setEncryptedText(findTestObject('null'), 'zgMKZ/4x+qIZ/2ovhLJ/rw==')

WebUI.setText(findTestObject('null'), '1816757')

WebUI.click(findTestObject('null'))

WebUI.click(findTestObject('null'))

WebUI.selectOptionByIndex(findTestObject('null'), 'Ready')

not_run: WebUI.clickImage(findTestObject('null'))

not_run: WebUI.click(findTestObject('null'))

not_run: WebUI.click(findTestObject('null'))

not_run: WebUI.click(findTestObject('null'))

not_run: WebUI.closeBrowser()

not_run: WebUI.rightClick(findTestObject('null'))

